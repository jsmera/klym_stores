import factory
from django.test import TestCase
from freezegun import freeze_time

from .models import Product
from .views import view_as_function


class ProductFactory(factory.Factory):
    class Meta:
        model = Product

    name = factory.Faker("name")
    price = factory.Faker("pydecimal", left_digits=5, right_digits=2, positive=True)


# Create your tests here.


class Wrapper:
    """
    This Wrapper request a petition from HTTP o (via-function) gRCP
    """

    def request(self, url, method="GET", protocol="http", **kwargs):
        if protocol == "http":
            return self.client.get(url)
        else:
            ## Call the function
            return view_as_function


class ProductTestCase(TestCase):
    def test_cached_rate(self):
        ProductFactory.build_batch(5)

        response = self.client.get("/api/products/?currency=COP")
        future = datetime.date.today() + relativetime(days=1)
        with freeze_time(future):
            response_future = self.client.get("/api/products/?currency=COP")
            self.assertNotEqual(response, response_future)

    def test_list_new_products(self):
        response = self.client.get("/api/products/?currency=COP")
        new_products = ProductFactory.build_batch(2)
        response_new = self.client.get("/api/products/?currency=COP")
        self.assertEqual(len(response), len(response_new) + 2)
        response_new_id = filter(lambda product: product.id, response_new)
        for product in new_products:
            self.assertIn(product.id, response_new_id)
        self.assertEqual(len(response), len(response_new) + 2)

    # TEST TODO
    # 1. Endpoint to create a product
    # 1. Endpoint to update price for a product
    # 1. Only allow to get supported currencies
    # 1. Compare the price of a product in two different currencies
