import decimal

import requests
from django.conf import settings
from django.core.cache import cache
from django.db.models import F
from django.http import JsonResponse
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from klym_stores.products import models as product_models
from klym_stores.products import serializers as product_serializers

# For migrate this code to gRPC covert all ModelViewSet to APIView
# One API peer method of ModelViewSet
# Example: CreateProductAPIView -> ModelViewSet.create
# Example: UpdateProductAPIView -> ModelViewSet.update
# Example: ListProductAPIView -> ModelViewSet.list


class CreateProductAPIView(viewsets.APIView):
    def get(self, request, *args, **kwargs):
        return self().get(request, *args, **kwargs)


# This return a function
view_as_function = CreateProductAPIView.as_view()


class CreateProductViewSet(viewsets.ModelViewSet):
    """
    Class based view to create a new product
    Serializer: CreateProductSerializer

    URL Pattern:
        POST /api/products/ Create a new product
        PUT /api/products/<id>/ Update the only-price of a product
        GET /api/products/ List all products
            To get the price in a different currency, you can pass the currency as a query parameter. Example: /api/products/?currency=COP
    """

    serializer_class = product_serializers.CreateProductSerializer
    queryset = product_models.Product.objects.all()

    def update(self, request, *args, **kwargs):
        """
        Custom update method to update the only-price of a product
        """
        serialized_product = product_serializers.UpdateProductSerializer(request.data)
        if serialized_product.is_valid():
            serialized_product.save()
            return super().update(request, *args, **kwargs)
        return JsonResponse({"error": serialized_product.errors}, status=400)

    def __make_endpoint_currency(self, currency: str) -> str:
        """
        Construct the endpoint to get the currency
        """
        base_url = settings.CURRENCY_SERVICE_URL
        return f"{base_url}/USD/to/{currency}"

    def __get_rate(self, currency: str) -> decimal.Decimal:
        """
        Get the rate of the currency
        """
        key = currency
        if cache.get(key):
            return cache.get(key)
        endpoint = self.__make_endpoint_currency(currency)
        response = requests.get(endpoint)
        json_response = response.json()
        float_rate = json_response.get("rate")
        rate = decimal.Decimal(float_rate)
        cache.set(key, rate, timeout=60)
        return rate

    def list(self, request, *args, **kwargs):
        """
        List all products. To get the price in a different currency, you can pass the currency as a query parameter. Example: /api/products/?currency=CLP
        """
        currency = request.query_params.get("currency", default="USD")
        if currency not in settings.CURRENCY_SUPPORT:
            return JsonResponse({"error": "Currency not supported"}, status=400)
        queryset = self.filter_queryset(self.get_queryset()).annotate(
            final_price=F("price") * self.__get_rate(currency)
        )

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = product_serializers.CatalogProductSerializer(page, many=True, context={"currency": currency})
            return self.get_paginated_response(serializer.data)

        serializer = product_serializers.CatalogProductSerializer(queryset, many=True, context={"currency": currency})
        return Response(serializer.data)
