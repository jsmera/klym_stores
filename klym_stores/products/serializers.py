import decimal

import requests
from django.conf import settings
from django.core.cache import cache
from rest_framework import serializers

from .models import Product


class CreateProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            "name",
            "price",
        ]


class UpdateProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            "price",
        ]


class CatalogProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            "id",
            "name",
            "price",
        ]

    def __make_endpoint_currency(self, currency: str) -> str:
        """
        Construct the endpoint to get the currency
        """
        base_url = settings.CURRENCY_SERVICE_URL
        return f"{base_url}/USD/to/{currency}"

    def __get_rate(self, currency: str) -> decimal.Decimal:
        """
        Get the rate of the currency
        """
        key = currency
        if cache.get(key):
            return cache.get(key)
        endpoint = self.__make_endpoint_currency(currency)
        response = requests.get(endpoint)
        json_response = response.json()
        float_rate = json_response.get("rate")
        rate = decimal.Decimal(float_rate)
        cache.set(key, rate, timeout=60)
        return rate

    def get_price(self, obj) -> decimal.Decimal:
        currency = self.context.get("currency", default="USD")
        if currency != "USD":
            endpoint = self.__make_endpoint_currency(currency)
            response = requests.get(endpoint)
            json_response = response.json()
            float_rate = json_response.get("rate")
            rate = decimal.Decimal(float_rate)
            return obj.price * rate
        return obj.price
