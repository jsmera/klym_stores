from django.db import models

# Create your models here.


class Product(models.Model):
    name = models.CharField(verbose_name="Nombre", max_length=100, blank=False, null=False)
    price = models.DecimalField(verbose_name="Precio", max_digits=10, decimal_places=2, blank=False, null=False)
