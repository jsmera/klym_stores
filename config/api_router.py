from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from klym_stores.products.views import CreateProductViewSet
from klym_stores.users.api.views import UserViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)
router.register("products", CreateProductViewSet)


app_name = "api"
urlpatterns = router.urls
